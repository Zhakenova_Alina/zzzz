package repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {


    @Query(value = "select * from student where id = 2", nativeQuery = true)
    Student getStudent();

    List<Student> findAllByGroupId(long group_id);

    List<Student> findAll();
}

