package model;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "groups")
public class Group {
    @Id
    private long id;

    @Column(name = "name")
    private String name ;

}
